package com.chillminds.wiproassesment

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.liveData
import com.chillminds.wiproassesment.common.Resource
import com.chillminds.wiproassesment.dto.FactItem
import com.chillminds.wiproassesment.repository.FactsRepository
import kotlinx.coroutines.Dispatchers
import org.koin.java.KoinJavaComponent.inject

class FactsViewModel(application: Application) : AndroidViewModel(application) {

    val factHeading = MutableLiveData<String>()
    val factItemList = MutableLiveData<ArrayList<FactItem>>().apply { value = arrayListOf() }

    private val repository: FactsRepository by inject(FactsRepository::class.java)

    /*
    * Fetching Facts From Remote Repo
    * */
    fun getFactsFromRemote() = liveData(Dispatchers.IO) {
        emit(Resource.loading(data = null))
        try {
            emit(
                Resource.success(
                    data = repository.getFacts()
                )
            )
        } catch (exception: Exception) {
            emit(Resource.error(data = null, message = exception.message ?: "errorMessage"))
        }
    }

}