package com.chillminds.wiproassesment

import com.chillminds.wiproassesment.repository.FactsRepository
import com.chillminds.wiproassesment.services.FactService
import com.chillminds.wiproassesment.services.FactServiceImplementation
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.android.ext.koin.androidApplication
import org.koin.core.module.Module
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory
import java.util.concurrent.TimeUnit

class AppModules {

    private val networkModule = module {

        single { provideOkHttpClient() }

        single { provideRetrofit(get(), getProperty("baseUrl")) }

        single { provideApiService(get()) }

        factory { FactServiceImplementation(get()) }

        factory { FactsRepository(get()) }

    }

    private val appModule = module {
        single { FactsViewModel(androidApplication()) }
    }

    fun getModules(): List<Module> {
        return listOf(appModule, networkModule)
    }

    private fun provideApiService(retrofit: Retrofit): FactService =
        retrofit.create(FactService::class.java)

    private fun provideRetrofit(client: OkHttpClient, baseUrl: String): Retrofit {
        return Retrofit.Builder()
            .baseUrl(baseUrl)
            .client(client)
            .addConverterFactory(
                GsonConverterFactory.create(
                    GsonBuilder().serializeNulls().create()
                )
            )
            .addConverterFactory(ScalarsConverterFactory.create())
            .build()
    }

    private fun provideOkHttpClient(): OkHttpClient {
        val builder = OkHttpClient.Builder().connectTimeout(60, TimeUnit.SECONDS)
            .readTimeout(60, TimeUnit.SECONDS)

        if (BuildConfig.DEBUG) {
            builder.addInterceptor(HttpLoggingInterceptor().apply {
                level = HttpLoggingInterceptor.Level.BODY
            })
        }

        return builder.build()
    }

}