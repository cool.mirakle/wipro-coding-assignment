package com.chillminds.wiproassesment.common

object Constants {

    const val WAITING_MESSAGE = "Please Wait..."

    const val COMMON_ERROR_MESSAGE = "Something Went Wrong. Try Again."

}