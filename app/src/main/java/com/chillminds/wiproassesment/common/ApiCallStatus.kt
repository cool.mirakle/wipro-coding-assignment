package com.chillminds.wiproassesment.common

/*
* Network API Call
* */
enum class ApiCallStatus {
    SUCCESS,
    ERROR,
    LOADING
}
