package com.chillminds.wiproassesment.repository

import com.chillminds.wiproassesment.services.FactServiceImplementation

class FactsRepository(private val helper: FactServiceImplementation) {
    suspend fun getFacts() = helper.getFacts()
}