package com.chillminds.wiproassesment

import android.app.Application
import androidx.multidex.MultiDex
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidFileProperties
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

class MainApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        MultiDex.install(this)
        startKoin {
            androidLogger()
            androidContext(this@MainApplication)
            androidFileProperties("applications.properties")
            koin.loadModules(AppModules().getModules())
        }
    }

}