package com.chillminds.wiproassesment.ui.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.chillminds.wiproassesment.FactsViewModel
import com.chillminds.wiproassesment.R
import com.chillminds.wiproassesment.common.ApiCallStatus
import com.chillminds.wiproassesment.common.Constants
import com.chillminds.wiproassesment.databinding.FragmentFactListBinding
import org.koin.android.ext.android.inject

class FactListFragment : Fragment() {

    lateinit var binding: FragmentFactListBinding
    private val viewModel: FactsViewModel by inject()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_fact_list, container, false)
        binding.viewModel = viewModel
        binding.lifecycleOwner = this
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.swipeRefreshLayout.setOnRefreshListener {
            callFactApi()
        }
    }

    /*
    * Call Api And Set into View Model
    * */
    private fun callFactApi() {
        viewModel.getFactsFromRemote().observe(viewLifecycleOwner, {
            when (it.status) {
                ApiCallStatus.LOADING -> {
                    Toast.makeText(context, Constants.WAITING_MESSAGE, Toast.LENGTH_SHORT)
                        .show()
                }
                ApiCallStatus.SUCCESS -> {
                    it.data?.let { response ->
                        viewModel.factHeading.postValue(response.title)
                        viewModel.factItemList.postValue(response.rows)
                    }
                    binding.swipeRefreshLayout.isRefreshing = false
                }
                ApiCallStatus.ERROR -> {
                    Toast.makeText(context, Constants.COMMON_ERROR_MESSAGE, Toast.LENGTH_SHORT)
                        .show()
                    binding.swipeRefreshLayout.isRefreshing = false
                }
            }
        })
    }

}