package com.chillminds.wiproassesment.ui.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.fragment.findNavController
import com.chillminds.wiproassesment.R
import com.chillminds.wiproassesment.FactsViewModel
import com.chillminds.wiproassesment.common.ApiCallStatus
import com.chillminds.wiproassesment.common.Constants
import org.koin.android.ext.android.inject

class SplashScreenFragment : Fragment() {

    private val viewModel: FactsViewModel by inject()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        return inflater.inflate(R.layout.splash_screen_fragment, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        callFactApi()
    }

    private fun callFactApi() {
        viewModel.getFactsFromRemote().observe(viewLifecycleOwner, {
            when (it.status) {
                ApiCallStatus.LOADING -> {
                    Toast.makeText(context, Constants.WAITING_MESSAGE, Toast.LENGTH_SHORT)
                        .show()
                }
                ApiCallStatus.SUCCESS -> {
                    it.data?.let { response ->
                        viewModel.factHeading.postValue(response.title)
                        viewModel.factItemList.postValue(response.rows)
                    }
                    findNavController().navigate(R.id.action_splashScreenFragment_to_factListFragment)
                }
                ApiCallStatus.ERROR -> {
                    Toast.makeText(context, Constants.COMMON_ERROR_MESSAGE, Toast.LENGTH_SHORT)
                        .show()
                }
            }
        })
    }

}