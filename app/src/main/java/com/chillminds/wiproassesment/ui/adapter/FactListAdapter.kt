package com.chillminds.wiproassesment.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.chillminds.wiproassesment.R
import com.chillminds.wiproassesment.databinding.FactListItemViewBinding
import com.chillminds.wiproassesment.dto.FactItem
import com.chillminds.wiproassesment.loadImage
import java.util.*

class FactListAdapter(private val factDataList: ArrayList<FactItem>?) :
    RecyclerView.Adapter<FactListAdapter.ViewHolder>() {

    class ViewHolder(private val binding: FactListItemViewBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(
            data: FactItem?
        ) {
            binding.factItem = data
            binding.factImageView.loadImage(data?.imageHref ?: "")
            binding.executePendingBindings()
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.fact_list_item_view,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return factDataList?.size ?: 0
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(factDataList?.get(position))
    }
}
