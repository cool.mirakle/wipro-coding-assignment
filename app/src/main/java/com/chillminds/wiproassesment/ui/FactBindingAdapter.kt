package com.chillminds.wiproassesment.ui

import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.chillminds.wiproassesment.dto.FactItem
import com.chillminds.wiproassesment.ui.adapter.FactListAdapter


@BindingAdapter("setFactAdapter", requireAll = false)
fun setFactAdapter(recyclerView: RecyclerView, factList: ArrayList<FactItem>?) {
    recyclerView.layoutManager =
        LinearLayoutManager(recyclerView.context, RecyclerView.VERTICAL, false)
    recyclerView.adapter =
        FactListAdapter(
            factDataList = factList
        )
}