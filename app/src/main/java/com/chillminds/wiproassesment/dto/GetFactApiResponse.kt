package com.chillminds.wiproassesment.dto

data class GetFactApiResponse(
    val title: String,
    val rows: ArrayList<FactItem>?
)
