package com.chillminds.wiproassesment.dto

data class FactItem(
    val title: String? = null,
    val description: String? = null,
    val imageHref: String? = null
)
