package com.chillminds.wiproassesment.services

import com.chillminds.wiproassesment.dto.GetFactApiResponse

class FactServiceImplementation(
    private val service: FactService,
) {

    suspend fun getFacts(): GetFactApiResponse = service.getFacts()

}