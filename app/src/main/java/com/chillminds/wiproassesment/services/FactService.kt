package com.chillminds.wiproassesment.services

import com.chillminds.wiproassesment.dto.GetFactApiResponse
import retrofit2.http.GET
import retrofit2.http.Headers

interface FactService {

    companion object {
        const val contentType = "Content-Type: application/json"
    }

    @Headers(contentType)
    @GET("s/2iodh4vg0eortkl/facts.json")
    suspend fun getFacts(): GetFactApiResponse
}