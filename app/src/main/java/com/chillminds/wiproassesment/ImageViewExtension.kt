package com.chillminds.wiproassesment

import android.widget.ImageView
import com.chillminds.wiproassesment.common.GlideApp

fun ImageView.loadImage(url: String) {
    GlideApp.with(context)
        .load(url)
        .placeholder(
            R.drawable.wipro
        )
        .into(this)
}